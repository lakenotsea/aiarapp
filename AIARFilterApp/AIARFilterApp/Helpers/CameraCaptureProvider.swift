//
//  FrameExtraction.swift
//  AIARFilterApp
//
//  Created by Elizaveta Alekseeva on 14/10/2018.
//  Copyright © 2018 elizavetaAlekseeva. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

protocol CameraCaptureProviderDelegate: class {
	func captured(image: UIImage)
}

public struct Filter {
	public struct FilterEffectParameter {
		public var key: String?
		public var value: Any?
		
		public init(_ key: String?, _ value: Any?) {
			self.key = key
			self.value = value
		}
	}
	
	public var filterName: String
	public var filterParams: [FilterEffectParameter]?
	
	public init(_ filterName: String, _ filterParams: [FilterEffectParameter]? = nil ) {
		self.filterName = filterName
	}
}

class CameraCaptureProvider: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
	private let captureSession = AVCaptureSession()
	private let sessionQueue = DispatchQueue(label: "session queue")
	
	private let position = AVCaptureDevice.Position.front
	private let quality = AVCaptureSession.Preset.high
	
	private var cameraPermissionGranted = false
	private var libraryPermissionGranted = false
	
	weak var delegate: CameraCaptureProviderDelegate?

	private let context = CIContext()
	
	private var currentFilter: Filter?
	
	override init() {
		super.init()
		checkCameraPermission()
		sessionQueue.async { [unowned self] in
			self.configureSession()
			self.captureSession.startRunning()
		}
	}
	
	func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
		guard let uiImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else { return }
		DispatchQueue.main.async { [unowned self] in
			self.delegate?.captured(image: uiImage)
		}
	}

	private func checkCameraPermission() { //TODO: move to permission manager
		switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
		case .authorized:
			cameraPermissionGranted = true
			break
		case .notDetermined:
			requestCameraPermission()
			break
		default:
			cameraPermissionGranted = false
			break
		}
	}
	
	func setCurrentFilter(filter: Filter?) {
		currentFilter = filter
	}
	
	private func requestCameraPermission() { //TODO: move to permission manager
		sessionQueue.suspend()
		AVCaptureDevice.requestAccess(for: AVMediaType.video) { [unowned self] granted in
			self.cameraPermissionGranted = granted
			self.sessionQueue.resume()
		}
	}
	
	private func configureSession() {
		guard cameraPermissionGranted else { return }
		captureSession.sessionPreset = quality
		guard let captureDevice = selectCaptureDevice(position) else { return }
		guard let captureDeviceInput = try? AVCaptureDeviceInput(device: captureDevice) else { return }
		guard captureSession.canAddInput(captureDeviceInput) else { return }
		captureSession.addInput(captureDeviceInput)
		let videoOutput = AVCaptureVideoDataOutput()
		videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer"))
		captureSession.addOutput(videoOutput)
		
		guard let connection = videoOutput.connection(with: AVFoundation.AVMediaType.video) else { return }
		guard connection.isVideoOrientationSupported else { return }
		guard connection.isVideoMirroringSupported else { return }
		connection.videoOrientation = .portrait
		connection.isVideoMirrored = position == .front
	}
	
	// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
	private func selectCaptureDevice(_ position: AVCaptureDevice.Position) -> AVCaptureDevice? {
		return AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified).devices.filter {
				($0 as AnyObject).position == position
			}.first
	}
	
	private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
		//Transform the sample buffer to a CVImageBuffer
		guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
		//Create a CIImage from the image buffer
		var ciImage = CIImage(cvPixelBuffer: imageBuffer)
		//remember captured extent
		let ciImageExtent = ciImage.extent
		
		//apply current filter is it is presented
		if let currentFilter = currentFilter,
			let filteredImage = applyFilter(ciImage: ciImage.clampedToExtent(), filter: currentFilter) {
			ciImage = filteredImage
		}
		//Create a CGImage from context
		guard let cgImage = context.createCGImage(ciImage, from: ciImageExtent) else { return nil }
		//Create and return the UIImage
		return UIImage(cgImage: cgImage)
	}
	
	private func applyFilter(ciImage: CIImage, filter: Filter) -> CIImage? {
		guard let ciFilter = CIFilter(name: filter.filterName) else { return nil }

		ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
		
		if let filterParams = filter.filterParams {
			filterParams.forEach { (filterParam) in
				if let filterEffectValue = filterParam.value,
					let filterEffectKey = filterParam.key {
					ciFilter.setValue(filterEffectValue, forKey: filterEffectKey)
				}
			}
		}

		return ciFilter.value(forKey: kCIOutputImageKey) as? CIImage
	}
}
