//
//  ViewController.swift
//  AIARFilterApp
//
//  Created by Elizaveta Alekseeva on 14/10/2018.
//  Copyright © 2018 elizavetaAlekseeva. All rights reserved.
//

import UIKit
import SnapKit
import Photos

class ViewController: UIViewController {

	private var cameraCaptureProvider: CameraCaptureProvider!
	
	@IBOutlet weak var imageView: UIImageView!
	
	private let filters: [Filter?] = [nil,
									  Filter("CIGaussianBlur",[Filter.FilterEffectParameter(kCIInputRadiusKey, 8.0)]),
									  Filter("CIComicEffect"),
									  Filter("CICrystallize")] // TODO: move to filter manager
	private var currentFilterIndex: Int = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		cameraCaptureProvider = CameraCaptureProvider()
		cameraCaptureProvider.delegate = self
		
		self.view.addSubview(takePhotoButton)
		
		self.takePhotoButton.snp.makeConstraints { (maker) in
			maker.bottom.equalTo(self.view.snp.bottom).inset(24)
			maker.centerX.equalTo(self.view.snp.centerX)
		}
	}
	
	lazy var takePhotoButton: RoundAnimatableButton = {
		let button = RoundAnimatableButton(frame: CGRect.zero)
		button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
		return button
	}()
	
	@objc func buttonClicked(_ sender: AnyObject?) {
		takePhotoButton.animate()
		
		guard let image = imageView.image else {
			return
		}
		
		PHPhotoLibrary.requestAuthorization { [weak self] status in
			guard status == .authorized else {
				self?.showNeedLibraryPermissionAlert()
				return
			}
			
			PHPhotoLibrary.shared().performChanges({
				PHAssetChangeRequest.creationRequestForAsset(from: image)
				
			}, completionHandler: { success, error in
			})
		}
	}
	
	private func showNeedLibraryPermissionAlert() {
		let alertController = UIAlertController(title: "Error", message:
			"Please authenticate access to photo library in settings", preferredStyle: UIAlertController.Style.alert)
		alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: nil))
		
		self.present(alertController, animated: true, completion: nil)
	}

	@IBAction func rightSwipeGestureHandler(_ gestureRecognizer : UISwipeGestureRecognizer) {
		let newCurrentFilterIndex = currentFilterIndex - 1
		if newCurrentFilterIndex >= 0 {
			currentFilterIndex = newCurrentFilterIndex
			cameraCaptureProvider.setCurrentFilter(filter: filters[currentFilterIndex])
		}
	}
	
	@IBAction func leftSwipeGestureHandler(_ sender: Any) {
		let newCurrentFilterIndex = currentFilterIndex + 1
		if newCurrentFilterIndex < filters.count {
			currentFilterIndex = newCurrentFilterIndex
			cameraCaptureProvider.setCurrentFilter(filter: filters[currentFilterIndex])
		}
	}
}

extension ViewController: CameraCaptureProviderDelegate {
	func captured(image: UIImage) {
		imageView.image = image
	}
}

