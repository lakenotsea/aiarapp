//
//  RoundAnimatableButton.swift
//  AIARFilterApp
//
//  Created by Elizaveta Alekseeva on 15/10/2018.
//  Copyright © 2018 elizavetaAlekseeva. All rights reserved.
//

import UIKit
import SnapKit

public class RoundAnimatableButton: UIButton {
	struct Appearance {
		let animationDuration: CFTimeInterval = 0.3
		let cornerRadius: CGFloat = 30.0
		let backgroundColor: UIColor = UIColor.cyan
		let width: CGFloat = 60.0
		let scaleAnimationValue: CGFloat = 1.5
	}
	
	private let appearance = Appearance()
	
	override public init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = appearance.backgroundColor
	
		self.snp.makeConstraints { (maker) in
			maker.width.equalTo(appearance.width)
			maker.height.equalTo(appearance.width)
		}
		
		self.layer.cornerRadius = appearance.cornerRadius
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func animate() {
		// View animations
		UIView.animate(withDuration: appearance.animationDuration,
					   animations: {
						self.transform = CGAffineTransform(scaleX: self.appearance.scaleAnimationValue, y: self.appearance.scaleAnimationValue)
		},
					   completion: { _ in
						UIView.animate(withDuration: self.appearance.animationDuration) {
							self.transform = CGAffineTransform.identity
						}
		})
	}
}
